#include <bits/stdc++.h>
using namespace std;
string encryption(string msg, int key) {
  string encryptmsg = "";
  char ch = msg[0];
  if (ch >= 'a' && ch <= 'z')
    ch = (ch + key - 'a') % 26 + 'a';
  else
    ch = (ch + key - 'A') % 26 + 'A';
  encryptmsg += ch;

  for (int i = 1; i < msg.length(); i++) {
    char ch = msg[i];
    if (ch >= 'a' && ch <= 'z') {
      key = msg[i - 1] - 'a';
      ch = (ch + key - 'a') % 26 + 'a';
      encryptmsg += ch;
    } else if (ch >= 'A' && ch <= 'Z') {
      key = msg[i - 1] - 'A';
      ch = (ch + key - 'A') % 26 + 'A';
      encryptmsg += ch;
    }
  }
  return encryptmsg;
}
string decryption(string msg, int key) {
  string decryptmsg = "";
  char ch = msg[0];
  cout << (int)(ch - key) << "\n";
  if (ch >= 'a' && ch <= 'z')
    ch = (ch - key - 'a') > 26 ? (ch - key - 'a')
                               : (ch - key - 'a' + 26) % 26 + 'a';
  else
    ch = (ch - key - 'A') > 26 ? (ch - key - 'A')
                               : (ch - key - 'A' + 26) % 26 + 'A';

  msg[0] = ch;
  for (int i = 1; i < msg.length(); i++) {
    char ch = msg[i];
    if (ch >= 'a' && ch <= 'z') {
      key = msg[i - 1] - 'a';
      ch = (ch - key - 'a') > 26 ? (ch - key - 'a')
                                 : (ch - key - 'a' + 26) % 26 + 'a';
      msg[i] = ch;
    } else if (ch >= 'A' && ch <= 'Z') {
      key = msg[i - 1] - 'A';
      ch = (ch - key - 'A') > 26 ? (ch - key - 'A')
                                 : (ch - key - 'A' + 26) % 26 + 'A';
      msg[i] = ch;
    }
  }
  return msg;
}
int main() {
  string msg;
  int key;
  cin >> msg;
  cin >> key;
  cout << "Encrypted Message is : \n" << encryption(msg, key) << endl;
  cout << "Decrypted Message is: " << decryption(msg, key) << endl;
  return 0;
}