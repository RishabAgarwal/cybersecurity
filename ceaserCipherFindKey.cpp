#include <bits/stdc++.h>
using namespace std;

int main() {
  cout << "Enter the message:\n";
  string msg, input;
  cin >> input;
  char ch;
  for (int key = 1; key <= 26; key++) {
    msg = input;
    for (int i = 0; msg[i] != '\0'; ++i) {
      ch = msg[i];
      if (ch >= 'a' && ch <= 'z') {
        ch = ch - key;
        if (ch < 'a') {
          ch = ch + 'z' - 'a' + 1;
        }
        msg[i] = ch;
      } else if (ch >= 'A' && ch <= 'Z') {
        ch = ch - key;
        if (ch < 'A') {
          ch = ch + 'Z' - 'A' + 1;
        }
        msg[i] = ch;
      }
    }
    cout << "Decrypted message with key  " << key << "is: " << msg << endl;
  }
}