#include <bits/stdc++.h>
using namespace std;
string encryption(string msg, int key) {
  char ch;
  for (int i = 0; msg[i] != '\0'; ++i) {
    ch = msg[i];
    if (ch >= 'a' && ch <= 'z') {
      ch = ch + key;
      if (ch > 'z') {
        ch = ch - 'z' + 'a' - 1;
      }
      msg[i] = ch;
    } else if (ch >= 'A' && ch <= 'Z') {
      ch = ch + key;
      if (ch > 'Z') {
        ch = ch - 'Z' + 'A' - 1;
      }
      msg[i] = ch;
    }
  }
  return msg;
}
string decryption(string msg, int key) {
  char ch;
  for (int i = 0; msg[i] != '\0'; ++i) {
    ch = msg[i];
    if (ch >= 'a' && ch <= 'z') {
      ch = ch - key;
      if (ch < 'a') {
        ch = ch + 'z' - 'a' + 1;
      }
      msg[i] = ch;
    } else if (ch >= 'A' && ch <= 'Z') {
      ch = ch - key;
      if (ch < 'A') {
        ch = ch + 'Z' - 'A' + 1;
      }
      msg[i] = ch;
    }
  }
  return msg;
}
int main() {
  string msg;
  int key;

  cin >> msg;
  cin >> key;

  cout << "Encrypted Message is : " << encryption(msg, key) << endl;
  cout << "Decrypted Message is: " << decryption(msg, key) << endl;
  return 0;
}