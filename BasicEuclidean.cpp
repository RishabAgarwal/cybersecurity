#include <bits/stdc++.h>
using namespace std;

int gcd(int a, int b) {
  if (a == 0) {
    cout << "\t" << b << "\t" << a << endl;
    return b;
  }
  cout << (b / a) << "\t" << b << "\t" << a << "\t" << (b % a) << endl;
  return gcd(b % a, a);
}

int main() {
  int a = 60, b = 25;
  cout << "Q\tr1\tr2\tr\n";
  gcd(a, b);

  return 0;
}
