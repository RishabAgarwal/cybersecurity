def multiplicativeInverse(a, b):
    print("q\tr1\tr2\tr\ts1\ts2\ts\tt1\tt2\tt")
    s1, s2, t1, t2, q = 1, 0, 0, 1, 0
    r1, r2 = a, b
    r, s, t = 0, 0, 0
    while r2 > 0:
        q, r = r1 // r2, r1 % r2
        s = s1 - q * s2
        t = t1 - q * t2
        print(q, r1, r2, r, s1, s2, s, t1, t2, t, sep="\t")
        s1, s2 = s2, s
        t1, t2 = t2, t
        r1, r2 = r2, r
    print("", r1, r2, "", s1, s2, "", t1, t2, "", sep="\t")
    if r1 != 0:
        print("Inverse doesn't exist")
        print("GCD({},{}) is {}".format(a, b, r1))
        return
    if t1 < 0:
        print("Multiplicative inverse of {} is {} or {}".format(a, t1, t1 + a))
    else:
        print("Multiplicative inverse of {} is {}".format(a, t1))


n, b = 26, 12
multiplicativeInverse(n, b)
