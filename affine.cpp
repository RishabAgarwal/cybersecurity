#include <bits/stdc++.h>
using namespace std;

string encryption(string m, int k1, int k2) {
  string c = "";
  for (int i = 0; i < m.length(); i++) {
    if (m[i] != ' ')
      c = c + (char)((((k1 * (m[i] - 'A')) + k2) % 26) + 'A');
    else
      c += m[i];
  }
  return c;
}
string decryption(string c, int k1, int k2) {
  string m = "";
  int a_inverse = 0;
  int flag = 0;
  for (int i = 0; i < 26; i++) {
    flag = (k1 * i) % 26;
    if (flag == 1) {
      a_inverse = i;
    }
  }
  for (int i = 0; i < c.length(); i++) {
    if (c[i] != ' ')
      m = m + (char)(((a_inverse * ((c[i] + 'A' - k2)) % 26)) + 'A');
    else
      m += c[i];
  }
  return m;
}
int main(void) {
  string msg;
  int k1, k2;
  cin >> msg;
  cin >> k1 >> k2;

  cout << "Encrypted Message is : " << encryption(msg, k1, k2) << endl;
  cout << "Decrypted Message is: " << decryption(msg, k1, k2);
  return 0;
}